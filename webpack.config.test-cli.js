const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, './test/cli/test.js'),

  output: {
    path: path.resolve(__dirname, './test/cli/build'),
    filename: 'test-cli.api.min.js',
    library: 'akomando-tree-test-cli',
    libraryTarget: 'umd',
  },

  mode: 'production',

  target: 'node',

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
};
