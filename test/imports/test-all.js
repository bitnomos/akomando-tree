/* global describe, it, before */
import { expect } from 'chai';
import akomandoTree from '../../dist/akomando-tree.api.min';

const doTests = async (cli = false, execSync = null) => {
  describe('#getIndex tests for the getIndex function', () => {
    let remoteSample;
    before((done) => {
      if (!cli) {
        akomandoTree.getIndex()
          .then((data) => {
            remoteSample = data;
            done();
          })
          .catch(err => new Error(err));
      } else {
        remoteSample = JSON.parse(execSync('akomando-tree get-index').toString().trim());
        done();
      }
    });

    it('must return a json object', () => {
      expect(remoteSample != null);
      expect(typeof remoteSample).equal('object');
    });
    it('the total element count must be equal to 307', () => {
      expect(remoteSample.total).equal(307);
    });
    it('the elements counted and contained in the "elements" array must be equal to 40', () => {
      expect(remoteSample.elements.length).equal(40);
    });
  });
};

export default doTests;
