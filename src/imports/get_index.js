/* eslint-disable no-underscore-dangle */
import { createAkomando } from 'akomando';
import {
  getElementsByTagNameUniversal,
  getIdentifierObject,
  getHierHeadings,
  normalizeLevel,
  setChildrenProperty,
  hierIdentifiersNestify,
} from '../utils/utils';
/**
 * This function creates the index of the Akoma Ntoso document and returns an
 * object structured as {@link akomandoTree.index}.
 * @private
 * @throws {error} forwards error thrown by akomando.
 * @param {string} aknString The string representing the Akoma Ntoso document.
 * @param {Object} configs The package configuratons.
 * @param {boolean} nestHierarchies if set to true the hierarchies in the
 * returned object will be nested (defalut false).
 * @param {function} localize The localization function.
 * @return {Object} Returns an object containing the index of the Akoma Ntoso
 * documnt. The result is structured as a {@link akomandoTree.index} JSON Object.
*/
const getIndex = async (aknString, configs, nestHierarchies, localize) => {
  try {
    // create the akomando object on the given Akoma Ntoso string
    const akomando = createAkomando({
      aknString,
    });

    // get the document type
    const docType = akomando.getDocType();

    // get the akomando configs for the document type
    // eslint-disable-next-line no-underscore-dangle
    const docTypeConfigs = akomando._config.docTypes[docType];
    const docMainContainer = docTypeConfigs.mainContainer.name;
    const { indexOn } = configs.docTypes[docType];

    // get the document DOM object
    const aknDom = akomando.getAkomaNtoso();

    // the object that will be returned
    const docIdentfiers = [];

    // the content element for the input document
    const contentElement = akomando
      ._config
      .docTypes[akomando.getDocType()]
      .hierarchies
      .contentElement
      .name;

    Object.keys(indexOn).forEach((key) => {
      if (indexOn[key]) {
        const elements = getElementsByTagNameUniversal(
          aknDom,
          key,
          akomando._akn._AKNDomNamespace,
        );
        for (let i = 0; i < elements.length; i += 1) {
          const identifierObject = localize(getIdentifierObject(elements[i]));
          identifierObject.level = 0;
          if (key === docMainContainer) {
            identifierObject.hierarchies = akomando.getHierIdentifiers();
            identifierObject.hierarchies.forEach((hier, k) => {
              normalizeLevel(hier, contentElement, []);
              // fix xpath, to be changed when the akomando issue will be fixed
              identifierObject.hierarchies[k].xpath = `/akomaNtoso[1]/${docType}[1]${hier.xpath}`;
              const anHier = akomando.getElementsFromXPath(hier.xpath);
              identifierObject.hierarchies[k].headings = getHierHeadings(anHier[0]);
              identifierObject.hierarchies[k] = localize(identifierObject.hierarchies[k]);
            });
          }
          docIdentfiers.push(identifierObject);
        }
      }
    });
    if (nestHierarchies) {
      const hierWithChildrenProp = setChildrenProperty(docIdentfiers);
      return hierIdentifiersNestify(hierWithChildrenProp);
    }
    return docIdentfiers;
  } catch (err) {
    throw new Error(err);
  }
};

export default getIndex;
