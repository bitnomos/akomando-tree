/**
 * Given a partition identifiers object it returns the same object with
 * a "localeLabel" field that contains the localized label for that partiotion.
 * @param {Object} partitionIdentifiers the partition identifier object.
 * @return {Object} the partitionIndentifiers object that was given as input with
 * a "localeLabel" field.
*/
const locale = (partitionIdentifiers) => {
  const newPartitionIndentifiers = partitionIdentifiers;
  if (!newPartitionIndentifiers.headings) {
    newPartitionIndentifiers.localeLabel = newPartitionIndentifiers.name;
  } else {
    const partitionHeadings = newPartitionIndentifiers.headings;
    newPartitionIndentifiers.localeLabel = partitionHeadings.num;
    if (partitionHeadings.heading !== '') {
      newPartitionIndentifiers.localeLabel = `${newPartitionIndentifiers.localeLabel} - ${partitionHeadings.heading}`;
    }
  }
  return newPartitionIndentifiers;
};

export default locale;
