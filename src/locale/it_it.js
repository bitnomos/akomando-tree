const partitionMap = {
  clause: headings => `clausola ${headings.num}`,
  section: headings => `sezione ${headings.num} - ${headings.heading}`,
  part: headings => `parte ${headings.num} - ${headings.heading} `,
  paragraph: headings => `comma ${headings.num}`,
  chapter: headings => `${headings.num} - ${headings.heading}`,
  title: headings => `${headings.num} - ${headings.heading}`,
  article: headings => `${headings.num} - ${headings.heading}`,
  book: headings => `${headings.num} - ${headings.heading}`,
  tome: headings => `${headings.num} - ${headings.heading}`,
  division: headings => `comma ${headings.num}`,
  list: () => 'punti',
  point: headings => `punto ${headings.num}`,
  indent: () => 'versetto',
  alinea: () => 'rubrica',
  rule: headings => `regola ${headings.num}`,
  subrule: headings => `sotto regola ${headings.num}`,
  proviso: headings => `provisio ${headings.num}`,
  subsection: headings => `sotto sezione ${headings.num}`,
  subpart: headings => `sotto parte ${headings.num}`,
  subparagraph: headings => `sotto paragrafo ${headings.num}`,
  subchapter: headings => `sotto capitolo ${headings.num}`,
  subtitle: headings => `sotto titolo ${headings.num}`,
  subdivision: headings => `sotto divisione ${headings.num}`,
  subclause: headings => `sotto clausola ${headings.num}`,
  sublist: headings => `sotto lista ${headings.num}`,
  level: headings => `livello ${headings.num}`,
  transitional: () => 'transizionale',
  hcontainer: () => 'contenitore',
  blockList: () => 'lettere',
  item: headings => `lettera ${headings.num}`,
  collectionBody: () => 'documenti',
  body: () => 'articolato',
  mainBody: () => 'contenuto',
  coverPage: () => 'copertina',
  preface: () => 'prefazione',
  preamble: () => 'preambolo',
  conclusions: () => 'conclusioni',
  attachments: () => 'allegati',
  header: () => 'header',
  judgmentBody: () => 'sentenza',
};

/**
 * Given a partition identifiers object it returns the same object with
 * a "localeLabel" field that contains the localized label for that partiotion.
 * @param {Object} partitionIdentifiers the partition identifier object.
 * @return {Object} the partitionIndentifiers object that was given as input with
 * a "localeLabel" field.
*/
const locale = (partitionIdentifiers) => {
  const newPartitionIndentifiers = partitionIdentifiers;
  if (!newPartitionIndentifiers.headings) {
    newPartitionIndentifiers.localeLabel = (partitionMap[newPartitionIndentifiers.name])
      ? partitionMap[newPartitionIndentifiers.name]()
      : newPartitionIndentifiers.name;
  } else {
    const partitionHeadings = newPartitionIndentifiers.headings;
    newPartitionIndentifiers.localeLabel = (partitionMap[newPartitionIndentifiers.name])
      ? partitionMap[newPartitionIndentifiers.name](partitionHeadings)
      : newPartitionIndentifiers.name;
  }
  return newPartitionIndentifiers;
};

export default locale;
