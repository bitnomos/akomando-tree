const partitionMap = {
  header: () => 'intestazione',
  judgmentBody: () => 'sentenza',
  introduction: () => 'introduzione',
  background: () => 'fatto',
  motivation: () => 'diritto',
  decision: () => 'dispositivo',
  division: headings => `sezione ${headings.num}`,
  paragraph: headings => `paragrafo ${headings.num}`,
  conclusions: () => 'conclusioni',
  attachments: () => 'allegati',
};

/**
 * Given a partition identifiers object it returns the same object with
 * a "localeLabel" field that contains the localized label for that partiotion.
 * @param {Object} partitionIdentifiers the partition identifier object.
 * @return {Object} the partitionIndentifiers object that was given as input with
 * a "localeLabel" field.
*/
const locale = (partitionIdentifiers) => {
  const newPartitionIndentifiers = partitionIdentifiers;
  if (!newPartitionIndentifiers.headings) {
    newPartitionIndentifiers.localeLabel = (partitionMap[newPartitionIndentifiers.name])
      ? partitionMap[newPartitionIndentifiers.name]()
      : newPartitionIndentifiers.name;
  } else {
    const partitionHeadings = newPartitionIndentifiers.headings;
    newPartitionIndentifiers.localeLabel = (partitionMap[newPartitionIndentifiers.name])
      ? partitionMap[newPartitionIndentifiers.name](partitionHeadings)
      : newPartitionIndentifiers.name;
  }
  return newPartitionIndentifiers;
};

export default locale;
