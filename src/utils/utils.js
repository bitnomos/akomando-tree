/**
 * Giving a text, it returns the cleaned text. It will remove all the unusefull spaces, line break,
 * tabs and so on.
 * @param {String} text the text to clean
 * @returns {String} the cleaned text
 */
export const cleanText = (text) => {
  const cleanedText = text.trim();
  return cleanedText;
};

/**
 * Get the hierachies contained in a set of elements
 * @param {String} docTypeMainContainer The name of the main container of a doc type
 * @param {String} hiersContentElement The name of the content element in hierarchies
 * @param {Array} hierBlockElements The block elements that can contain other structures
 * (quotedStructure) in hierarchies
 * @param {Array} nodes The set of nodes in which hierarchies must be found
 * @param {Array} hierarchiesNames The names of the hierarchies that may be contained
 * in a specific doc type
 * @param {Array} hiers The returning array that is filled recursively with found hierarchies
 * @return {Array} An array containing all hierarchies that have been found inside
 * the supplied node set
*/
export const getDocumentHiers = (
  docTypeMainContainer,
  hiersContentElement,
  hierBlockElements,
  nodes,
  hierarchiesNames,
  hiers = [],
) => {
  for (let i = 0; i < nodes.length; i += 1) {
    const { nodeName } = nodes[i];
    if (nodeName === docTypeMainContainer || hierarchiesNames.includes(nodeName)) {
      if (nodeName !== docTypeMainContainer
            && nodeName !== hiersContentElement
            && !hierBlockElements.includes(hierBlockElements)) {
        hiers.push(nodes[i]);
      }
      if (nodes[i].hasChildNodes()) {
        const children = nodes[i].childNodes;
        getDocumentHiers(
          docTypeMainContainer,
          hiersContentElement,
          hierBlockElements,
          children,
          hierarchiesNames,
        );
      }
    }
  }
  return hiers;
};

/**
 * Get the previous node sibling of a given node
 * @param {Object} node The node whose previous sibling is required
 * @return {Object} The previous sibling of the node
*/
export const getPreviousSibling = (node) => {
  let x = node.previousSibling;
  while (x && x.nodeType !== 1) {
    x = x.previousSibling;
  }
  return x;
};

/**
 * Get the position of a node inside its parent
 * @param {Object} node The node to inspect
 * @param {Object} [position=1] The initial position to start the recursion
 * @return {Integer} The node poistion
*/
export const getNodePosition = (node, position = 0) => {
  let newPosition = position + 1;
  const previousSibling = getPreviousSibling(node);
  const nodePrevious = (previousSibling) || null;
  if (nodePrevious && nodePrevious.nodeName === node.nodeName) {
    newPosition = getNodePosition(nodePrevious, newPosition);
  }
  return newPosition;
};

/**
 * Given an element it returns its xpath
 * @param {Object} node The node that must be described
 * @return {String} the xpath of the node as a string
*/
export const getXpath = (node) => {
  const nodes = [];
  while (node && node.nodeType !== node.DOCUMENT_NODE) {
    nodes.push({
      name: node.nodeName,
      pos: getNodePosition(node, 0),
    });
    // eslint-disable-next-line no-param-reassign
    node = node.parentNode;
  }
  const paths = nodes
    .reverse()
    .map(aNode => `${aNode.name}[${aNode.pos}]`);
  return `/${paths.join('/')}`;
};

/**
 * Given an element it returns the {@link akomando.identifier} object describing the element
 * @param {Object} node The node that must be described
 * @return {Object} The {@link akomando.identifier} object describing the node
*/
export const getIdentifierObject = node => ({
  name: node.nodeName,
  eId: node.hasAttribute('eId') ? node.getAttribute('eId') : '',
  wId: node.hasAttribute('wId') ? node.getAttribute('wId') : '',
  GUID: node.hasAttribute('GUID') ? node.getAttribute('GUID') : '',
  xpath: getXpath(node),
});

/**
 * Improved dom.getElementsByTagName method that you can call with or without namespace
 * @param {String} context the context in which elments that must be found
 * @param {String} elementName the name of the elments that must be found
 * @param {String} namespace the namespace
 * @return {Object} the nodeList containing the results of the query
*/
export const getElementsByTagNameUniversal = (
  context,
  elementName,
  namespace = null,
) => {
  if (context) {
    return (namespace)
      ? context.getElementsByTagNameNS(namespace, elementName)
      : context.getElementsByTagName(elementName);
  }
  return [];
};

/**
 * Get the headings of a hierarcy by extracting them from the num, heading and subheading of
 * the hierarchy.
 * @param {Object} hier the DOM node of the hiearchy
 * @return {Object} an object containing the headings of the hierarchy.
 */
export const getHierHeadings = (hier) => {
  const hierHeadings = {
    num: '',
    heading: '',
    subheading: '',
  };
  if (hier.hasChildNodes()) {
    const hierChildren = hier.childNodes;
    for (let i = 0; i < hierChildren.length; i += 1) {
      const hierChildName = hierChildren[i].nodeName;
      if (hierChildName === 'num') {
        hierHeadings.num = cleanText(hierChildren[i].textContent);
      } else if (hierChildName === 'heading') {
        hierHeadings.heading = cleanText(hierChildren[i].textContent);
      } else if (hierChildName === 'subheading') {
        hierHeadings.subheading = cleanText(hierChildren[i].textContent);
      }
    }
  }
  return hierHeadings;
};

/**
 * Normalizes the levels of the hierachies according by decreasing the level
 * if the xpath contains the content element or one of the elements contained in the
 * exlude parameter.
 * @param {Object} hierIdentifiers the hier identifiers of a partition.
 * @param {string} contentElement the name of the content element used in the input
 * Akoma Ntoso document.
 * @param {array} exclude the elements to exlude.
 * @return {Object} a new hier identifiers object with normalized levels.
 */
export const normalizeLevel = (hierIdentifiers, contentElement, exclude) => {
  const hierIdentifiersCopy = hierIdentifiers;
  const hierXpath = hierIdentifiersCopy.xpath;
  if (hierXpath.includes(contentElement)) {
    hierIdentifiersCopy.level -= 1;
  }
  exclude.forEach((item) => {
    if (hierXpath.includes(item)) {
      hierIdentifiersCopy.level -= 1;
    }
  });
  return hierIdentifiersCopy;
};

/**
 * Given identifiers objects of two partitions, checks if the second one is child
 * of the first one
 * @param {Object} partition the indentifiers of the partitions that is supposed to be
 * the parent one.
 * @param {Object} needle the identifiers of the partition that is supposed to be the child
 * on.
 * @return {boolean} true if the partition in the second argument is child of the partion in
 * the first argument
 */
export const isChild = (partition, needle) => {
  const parentXpath = partition.xpath;
  const parentLevel = partition.level;
  const childXpath = needle.xpath;
  const childLevel = needle.level;
  if (childXpath.includes(parentXpath) && childLevel - parentLevel === 1) {
    return true;
  }
  return false;
};

export const setChildrenProperty = (hierIdentifiers) => {
  const hiersWithChildrenProp = [];
  while (hierIdentifiers.length > 0) {
    const currentHier = hierIdentifiers.shift();
    if (!currentHier.hierarchies) {
      currentHier.children = [];
    } else {
      currentHier.children = setChildrenProperty(currentHier.hierarchies);
    }
    hiersWithChildrenProp.push(currentHier);
  }
  return hiersWithChildrenProp;
};

export const hierIdentifiersNestify = (hierIdentifiers) => {
  const nestedHierarchies = [];

  // prendi il primo e il resto
  const firstElement = hierIdentifiers[0];
  const restArray = hierIdentifiers.slice(1);
  const nonChild = [];

  // confronta il primo con il resto
  restArray.forEach((item) => {
    // se uno del resto è figlio del primo inseriscilo tra i figli del primo e rimuovi dal resto
    if (isChild(firstElement, item)) {
      firstElement.children.push(item);
    } else {
      nonChild.push(item);
    }
  });

  // lancia sui figli del primo
  if (firstElement.children.length > 0) {
    firstElement.children = hierIdentifiersNestify(firstElement.children);
  }

  // inserisci il primo nel risultato
  nestedHierarchies.push(firstElement);
  const retArray = nestedHierarchies.concat(nonChild);
  /* if (nonChild.length > 0) {
    return nestedHierarchies.concat(hierIdentifiersNestify(nonChild));
  } */
  return retArray;
};
