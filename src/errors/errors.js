const errors = {
  getError({
    errorName,
    parName,
  } = {}) {
    switch (errorName) {
      case 'glob.inputIsNotAString':
        return `The given ${parName} option is not a string`;
      case 'glob.paramRequired':
        return `The option ${parName} is mandatory`;
      case 'glob.localeNotSupported':
        return `The locale ${parName} is not supported`;
      case 'glob.localeNotRecognized':
        return 'The input locale is neither a string nor a function';
      default:
        return 'Unknown error';
    }
  },
};

export default errors;
