import { typeCheck } from 'type-check';
import getIndex from '../imports/get_index';

// the js file that will contain the errors that the package may throw
import errors from '../errors/errors';

// the default configurations file
import config from '../configs/configs.json';

// the default localization file
import locale from '../locale/default';

// the italian locale
import it from '../locale/it_it';

// the locale for the italian costitutional court
import itJudgmentCorteCostituzionale from '../locale/it_it_judgment_corte_costituzionale';

/**
  * <p>
  * This package supplies functionalities related to the tree
  * of Akoma Ntoso documents.
  *
  * Currently, it supplies the following functionalities:
  *  <ul>
  *   <li>
  *     <strong>getIndex</strong>: returns a json object representing the index of the document.
  *   </li>
  *  </ul>
  * </p>
  *
  * @module akomandoTree
*/
const akomandoTree = {

  // the locale object
  localize: locale,

  /**
   * Set the locale that must be used for the generated index.
   * The locale could be a string matching the name of one of the locales included
   * in the 'locale' folder, or a function. See the locale foldar for examples on how
   * to create a new locale.
   * @param {object} locale a locale object formatted as the
   * default one (see /src/locale/default.js)
   * @throws {localeNotSupported} when the given locale is a string not incuded in default
   * ones stored in the locale directory
   * @throws {localeNotRecognized} when the given locale is neither a string nor a function
   * @return {void}
  */
  set locale(aLocale) {
    if (typeCheck('String', aLocale)) {
      if (this.locales.includes(aLocale)) {
        if (aLocale === 'it_it') {
          this.localize = it;
        } else if (aLocale === 'it_it_judgment_corte_costituzionale') {
          this.localize = itJudgmentCorteCostituzionale;
        }
      } else {
        throw new Error(errors.getError({
          errorName: 'glob.localeNotSupported',
          parName: aLocale,
        }));
      }
    } else if (typeof aLocale === 'function') {
      this.localize = aLocale;
    } else {
      throw new Error(errors.getError({
        errorName: 'glob.localeNotRecognized',
      }));
    }
  },

  /**
    * This function returns the tree of the given Akoma Ntoso document
    * @memberof module:akomandoTree
    * @async
    * @throws {paramRequired} throws an error when the aknString option was
    * not set
    * @throws {inputIsNotAString} throws an error when the given aknString option
    * is not a string
    * @param {object} options the options for the function
    * @param {string} options.aknString the string representing the Akoma Ntoso document
    * @param {string} options.configs the configurations for the package.
    * @param {boolean} options.nestHierarchies [false] if set to true the hierarchies in the
    * returned object will be nested (defalut false).
    * If none is send then it uses the default configuration file.
    * The configuration file must be compliant the {@link toBeDone} FormData.
    * @return {object} a JSON object containing the index of the Akoma Ntoso document
    * @example <caption>NODE (ES6)</caption>
    * import { akomandoTree } from 'akomando-tree';
    *
    * const myAkomaNtosoStream = fs.readFileSync('./my-akomantoso.xml');
    * const aknString = myAkomaNtosoStream.toString();
    * akomandoTree.getIndex({
    *  aknString,
    * }).then((data) => {
    *   // Logs the JSON object containing the index of the Akoma Ntoso document
    *   console.log(data);
    * }).catch(console.log);
    *
    * @example <caption>BROWSER</caption>
    * <html>
    *    <head>
    *       <title>akomando-tree example</title>
    *    </head>
    *    <body>
    *       <script src="https://code.jquery.com/jquery-3.1.1.min.js"
    *               integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    *               crossorigin="anonymous"></script>
    *       <script src="akomando-tree.api.min.js" charset="UTF-8"></script>
    *       <script>
    *          // load the content of an AkomaNtoso file in some way. Here I use jQuery.
    *          var myAkomaNtosoString = $.ajax({
    *             url: 'my-akomantoso.xml',
    *             async: false
    *          }).responseText;

    *          akoma =  akomando.createAkomando({
    *             aknString: myAkomaNtosoString,
    *          });
    *
    *          // Logs the number of all elements contained in the current AkomaNtoso document
    *          console.log(myAkomando.countDocElements());
    *
    *          // Logs the number of all 'ref' elements contained in the current AkomaNtoso document
    *          console.log(myAkomando.countDocElements({
    *             filterByName: 'ref',
    *          }));
    *       </script>
    *    </body>
    * </html>
    * @example <caption>COMMAND LINE (linux)</caption>
    * // returns a JSON object containing the index of the given AkomaNtoso document
    * $ akomando-tree get-index <path/to/file>
    *
    * @example <caption>COMMAND LINE (MS-DOS)</caption>
    * // returns a JSON object containing the index of the given AkomaNtoso document
    * $ akomando-tree get-index "path\to\file"
  */
  async getIndex({
    aknString,
    configs = config,
    nestHierarchies = false,
  } = {}) {
    if (!aknString) {
      throw new Error(errors.getError({
        errorName: 'glob.paramRequired',
        parName: 'aknString',
      }));
    }
    if (!typeCheck('String', aknString)) {
      throw new Error(errors.getError({
        errorName: 'glob.inputIsNotAString',
        parName: 'aknString',
      }));
    }
    // await the akomando object created on the given string
    const index = await getIndex(aknString, configs, nestHierarchies, this.localize);
    return index;
  },
  /**
   * These comments specify that the getIndex method must be allowed in the CLI as
   * a command and they specify configurations options fot the cli command.
   * @cli method
   * @cli method.name getIndex
   * @cli method.command get-index
   * @cli method.description returns the index of the given Akoma Ntoso file
   * @cli method.options
   * @cli method.options.1.param aknString
   * @cli method.options.1.option -f, --file [path]
   * @cli method.options.1.required true
   * @cli method.options.1.type file-path
   * @cli method.options.1.description the path of the Akoma Ntoso file
  */
};

// set the available locales
Object.defineProperty(akomandoTree, 'locales', {
  value: ['it_it', 'it_it_judgment_corte_costituzionale'],
  writable: false,
});

export default akomandoTree;
