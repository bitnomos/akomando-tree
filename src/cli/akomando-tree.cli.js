#!/usr/bin/env node
/* global CONFIGS_CLI */
import program from 'commander';
import chalk from 'chalk';
import { pd } from 'pretty-data2';

// replace all akomandoTree instances with the name of your package
import akomandoTree from '../api/akomando-tree.api';
import packConfigs from '../../package.json';

/**
 * All the methods of your package's api are automatically imported according
 * to CONFIG_CLI global object that are created by webpack before creating the
 * cli distribution file.
 * Check the file ./webpack.config.build-cli.before.js to see how the object
 * is created
*/
const cliConfs = CONFIGS_CLI;

// start to wrap api methods (if required, as said in above comment)
program.version(packConfigs.version);
program.description(packConfigs.description);

const resolveAction = async (method, options) => {
  const methodOptions = {};
  if (method.options) {
    method.options.forEach((opt) => {
      methodOptions[opt.param] = options[opt.param];
    });
  }
  try {
    const results = await akomandoTree[method.name](methodOptions);
    const formattedResults = (() => {
      switch (typeof results) {
        case 'object':
          return (results.toString().startsWith('<'))
            ? chalk.green.bold(pd.xml(results.toString()))
            : chalk.green.bold(pd.json(results));
        default:
          return chalk.green.bold(results.toString());
      }
    })();
    process.stdout.write(chalk.green.bold(`${formattedResults}\n`));
    return formattedResults;
  } catch (err) {
    process.stdout.write(chalk.red.bold(`${err}\n`));
    return new Error(err);
  }
};

cliConfs.configsCli.methods.forEach((meth) => {
  const cmd = program.command(meth.command);
  cmd.description(meth.description);

  if (meth.options) {
    meth.options.forEach((opt) => {
      cmd.option(`${opt.option}, ${opt.description}`);
    });
  }

  cmd.action(options => resolveAction(meth, options));
});

resolveAction();

program.parse(process.argv);
if (program.args.length === 0) program.help();
