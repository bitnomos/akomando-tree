/**
 * Use this file to test your code while developing.
 * Run the file by launching 'npm run dev'.
*/

import fs from 'fs';
import akomandoTree from './src/api/akomando-tree.api';

// const aknString = fs.readFileSync('./test/akn-docs/01082957-ft.akn.xml');
 const aknFile = fs.readFileSync('./test/akn-docs/judgment_corte_costituzionale.xml');
// const aknFile = fs.readFileSync('./test/akn-docs/main-1.akn.xml');
const aknString = aknFile.toString();
const configs = {
  docTypes: {
    amendmentList: {
      indexOn: {
        coverPage: true,
        preface: true,
        preamble: true,
        collectionBody: true,
        conclusions: true,
        attachments: true,
      },
    },
    officialGazette: {
      indexOn: {
        coverPage: true,
        preface: true,
        preamble: true,
        collectionBody: true,
        conclusions: true,
        attachments: true,
      },
    },
    documentCollection: {
      indexOn: {
        coverPage: true,
        preface: true,
        preamble: true,
        collectionBody: true,
        conclusions: true,
        attachments: true,
      },
    },
    act: {
      indexOn: {
        coverPage: true,
        preface: true,
        preamble: true,
        body: false,
        conclusions: true,
        attachments: true,
      },
    },
  },
};

/* akomandoTree.getIndex({
  aknString: aknString.toString(),
}).then((data) => {
  console.log(data);
}).catch(console.log); */

(async () => {
  akomandoTree.locale = 'it_it_judgment_corte_costituzionale';
  const aknIndex = await akomandoTree.getIndex({
    aknString,
    nestHierarchies: true,
  });
  console.log(JSON.stringify(aknIndex));
})();
