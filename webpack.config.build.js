const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, './src/api/akomando-tree.api.js'),

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'akomando-tree.api.min.js',
    library: 'akomandoTree',
    libraryTarget: 'umd',
    umdNamedDefine: true,
  },
  mode: 'production',
  target: 'node',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
};
